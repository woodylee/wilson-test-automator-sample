
class CloudType:

    def __init__(self, ui_object, config_object):
        self.main_window = ui_object

        self.config = config_object
        self.cloud_type = self.config.cloud_type

    def __on_radio_button_prd_toggled(self):
        if self.main_window.radioButton_prd.isChecked() == True:
            print(" PRD is selected")
            self.config.config.set('DEFAULT', 'cloud_type', 'PRD')
            self.config.write_config()
        else:
            print(" PRD is unselected")

    def __on_radio_button_stg_toggled(self):
        if self.main_window.radioButton_stg.isChecked() == True:
            print(" STG is selected")
            self.config.config.set('DEFAULT', 'cloud_type', 'STG')
            self.config.write_config()
        else:
            print(" STG is unselected")

    def init_cloud_type_radio_button(self):
        if self.cloud_type == 'PRD':
            self.main_window.radioButton_prd.setChecked(True)
        elif self.cloud_type == 'STG':
            self.main_window.radioButton_stg.setChecked(True)

    def register_cloud_type_radio_button(self):
        self.main_window.radioButton_prd.toggled.connect(self.__on_radio_button_prd_toggled)
        self.main_window.radioButton_stg.toggled.connect(self.__on_radio_button_stg_toggled)
