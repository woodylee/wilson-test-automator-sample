
class RunTest:
    def __init__(self, ui_object, config_object):
        self.main_window = ui_object
        self.config = config_object

    def __on_start_test_button_clicked(self):
        self.main_window.pushButton_test_start.setDisabled(True)
        self.main_window.pushButton_test_stop.setEnabled(True)
        print("test start")

    def __on_stop_test_button_clicked(self):
        self.main_window.pushButton_test_start.setEnabled(True)
        self.main_window.pushButton_test_stop.setDisabled(True)
        print("test stop")

    def register_run_test_button(self):
        self.main_window.pushButton_test_start.setEnabled(True)
        self.main_window.pushButton_test_stop.setDisabled(True)
        self.main_window.pushButton_test_start.clicked.connect(self.__on_start_test_button_clicked)
        self.main_window.pushButton_test_stop.clicked.connect(self.__on_stop_test_button_clicked)

