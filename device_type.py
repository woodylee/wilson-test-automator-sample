device_list = ["Omni", "Element", "Surface Mount", "Rev2"]
device_code_list = ["awair-omni", "awair-element", "awair-surface-mount", "awair-r2"]

class DeviceType:

    def __init__(self, ui_object, config_object):
        self.main_window = ui_object
        self.config = config_object
        self.device_type = self.config.device_type

    def __on_device_type_combo_box_changed(self):
        self.device_type = self.main_window.comboBox_device_type.currentText()
        self.config.device_type = self.device_type
        self.config.config.set('DEFAULT', 'device_type', self.device_type)
        self.config.write_config()

    def init_device_type_combo_box(self):
        self.main_window.comboBox_device_type.addItems(device_list)
        device_index = self.main_window.comboBox_device_type.findText(self.device_type)
        self.main_window.comboBox_device_type.setCurrentIndex(device_index)

    def register_device_type_combo_box(self):
        self.main_window.comboBox_device_type.currentIndexChanged.connect(self.__on_device_type_combo_box_changed)

    @staticmethod
    def get_device_code(device_type_string):
        return device_code_list[device_list.index(device_type_string)]
