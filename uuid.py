
class Uuid:

    def __init__(self, ui_object, config_object):
        self.main_window = ui_object

        self.config = config_object
        self.uuid = self.config.uuid

    def __on_uuid_line_edit_changed(self):
        self.uuid =self.main_window.lineEdit_uuid.text()
        self.config.config.set('DEFAULT', 'uuid', self.uuid)
        self.config.write_config()

    def init_uuid_line_edit(self):
        if (self.uuid == ''):
            self.main_window.lineEdit_uuid.setPlaceholderText("12345")
        else:
            self.main_window.lineEdit_uuid.setText(self.uuid)

    def register_uuid_line_edit(self):
        self.main_window.lineEdit_uuid.textChanged.connect(self.__on_uuid_line_edit_changed)
