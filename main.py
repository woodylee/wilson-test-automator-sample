import os
import sys

from PyQt5 import QtWidgets, QtGui
from ui_main_window import Ui_MainWindow
from config import Config
from cloud_type import CloudType
from device_type import DeviceType
from uuid import Uuid
from run_test import RunTest

if __name__ == '__main__':
    os.environ['QT_MAC_WANTS_LAYER'] = '1'
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()

    # display Wilson.. :(
    ui.label_wilson.setPixmap(QtGui.QPixmap("wilson.png"))
    ui.label_wilson.setScaledContents(True)

    config = Config()
    config.load_config()

    cloud_type = CloudType(ui, config)
    cloud_type.init_cloud_type_radio_button()
    cloud_type.register_cloud_type_radio_button()

    device_type = DeviceType(ui, config)
    device_type.init_device_type_combo_box()
    device_type.register_device_type_combo_box()

    uuid = Uuid(ui, config)
    uuid.init_uuid_line_edit()
    uuid.register_uuid_line_edit()

    run_test = RunTest(ui, config)
    run_test.register_run_test_button()

    sys.exit(app.exec_())
