# Wilson QA Test Automator 
![winson](wilson-small.png)

## Prerequisite
* Install Python IDE PyCharm Community https://www.jetbrains.com/pycharm/download/#section=mac  
* Install QT5 by brew https://gist.github.com/sigmadream/45050b2efbbd64582487    

## How to Modify UI 
* Open QT creator
* Drag & Drop ui file (ui_main_window.ui)
* Save, and type 
```
pyuic5 ui_main_window.ui -o ui_main_window.py
```
** Note: Do not modify ui_main_window.ui manually 

