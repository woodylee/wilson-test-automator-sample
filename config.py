import os
import sys
import configparser

#config_file_path = sys.path[0] + '/config.ini'
config_file_path = 'config.ini'

class Config:
    config = configparser.ConfigParser()

    def __init__(self):
        print("config class constructed")

    def write_config(self):
        self.config.write(open(config_file_path, 'w'))

    def load_config(self):
        if not os.path.exists(config_file_path):
            self.config['DEFAULT'] = {'cloud_type': 'PRD',
                                      'device_type': 'Omni',
                                      'uuid': '',
                                      'ota_target1': '',
                                      'ota_target2': '',
                                      'duration': 3,
                                      'repeat_count': 20
                                      }
            self.write_config()
        else:
            self.config.read(config_file_path)

        self.cloud_type = self.config.get('DEFAULT', 'cloud_type')
        self.device_type = self.config.get('DEFAULT', 'device_type')
        self.uuid = self.config.get('DEFAULT', 'uuid')
        self.ota_target1 = self.config.get('DEFAULT', 'ota_target1')
        self.ota_target2 = self.config.get('DEFAULT', 'ota_target2')
        self.duration = self.config.get('DEFAULT', 'duration')
        self.repeat_count = self.config.get('DEFAULT', 'repeat_count')

        print('Cloud type: ' + self.cloud_type)
        print('device type: ' + self.device_type)
        print('uuid: ' + self.uuid)
